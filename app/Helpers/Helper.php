<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Log;

class Helper
{
    /**
     * Revisa si un elemento no esta definido o tiene un valor vacio
     *
     * @param mixed $data
     * @return boolean
     */
    public static function isNotSetOrEmpty($array, $key)
    {
        if (!isset($array[$key])) {
            return true;
        } elseif (empty($array[$key])) {
            return true;
        }

        return false;
    }

    /**
     * Implement array_filter recursive
     *
     * @param  array $array
     * @return array
     */
    public static function arrayFilterRecursive($array)
    {
        foreach ($array as &$value) {
            if (is_array($value)) {
                $value = static::array_filter_recursive($value);
            }
        }

        return array_filter($array);
    }

    /**
     * Convierte el array de los errores del model a una cadena de texto
     *
     * @param Array $arrayErrors Errores del modelo
     * @param string $join Default .
     * @return String
     */
    public static function errorsToString($arrayErrors, $join = ". ")
    {
        return array_reduce($arrayErrors, function ($result, $item) use ($join) {
            return $result . (
                empty($item) ? '' :
                (is_array($item) ? static::errorsToString($item) : ' '.$item . $join)
            );
        });
    }

    /**
     * Return the message of the exception if app.debug is true
     *
     * @param  Exception $e
     * @return string
     */
    public static function getMessageIfDebug($e)
    {
        Log::debug($e->getFile() . ' [' . $e->getLine() .']: ' . $e->getMessage());
        Log::debug(request()->input());
        Log::debug($e->getTraceAsString());

        if (config('app.debug')) {
            return $e->getFile() . ' [' . $e->getLine() .']: ' . $e->getMessage();
        }

        /**
         * Error Code
         * 422 = Validación
         * 404 = No encontrado
         * 403 = Acceso denegado
         */
        if ($e->getCode() == 422 || $e->getCode() == 404 || $e->getCode() == 403) {
            return $e->getMessage();
        }

        return '';
    }

    /**
     * Elimina espacios al inicio y final de la cadena,
     * elimina dobles espacios o más dentro de la cadena
     *
     * @param  string $value
     * @return string
     */
    public static function trim($value)
    {
        return preg_replace("'\s+'", ' ', trim($value));
    }

    /**
     * Elimina espacios al inicio y final de la cadena,
     * elimina dobles espacios o más dentro de la cadena
     * contenidas en un arreglo
     *
     * @param  array $data
     * @return array
     */
    public static function trimArray($data)
    {
        return collect($data)->map(function ($value, $key) {
            // Si el valor es una cadena
            if (is_string($value)) {
                // Elimina espacios al inicio y final de la cadena
                // Elimina dobles espacios o más dentro de la cadena
                return [$key => static::trim($value)];
            }

            return $value;
        })->toArray();
    }

    /**
     * Elimina espacios al inicio y final de la cadena,
     * elimina dobles espacios o más dentro de la cadena y
     * elimina etiquetas HTML y PHP
     *
     * @param  string $value
     * @return string
     */
    public static function stripTags($value)
    {
        return strip_tags(static::trim($value));
    }

    /**
     * Elimina espacios al inicio y final de la cadena,
     * elimina dobles espacios o más dentro de la cadena,
     * elimina etiquetas HTML y PHP
     * contenidos en un arreglo
     *
     * @param  array $data
     * @return array
     */
    public static function stripTagsArray($data)
    {
        return collect($data)->map(function ($value, $key) {
            // Si el valor es una cadena
            if (is_string($value)) {
                // Elimina espacios al inicio y final de la cadena
                // Elimina dobles espacios o más dentro de la cadena
                // Elimina etiquetas HTML y PHP
                return static::stripTags($value);
            }

            return $value;
        })->toArray();

        return $result;
    }

    /**
     * Obtiene la cantidad en kilobytes de una expresion en cadena,
     * ejemplo 2M = 2048
     *
     * @param  string $val
     * @return int
     */
    public static function returnKilobytes($val)
    {
        $val = trim($val);
        $last = strtolower($val[strlen($val)-1]);

        switch ($last) {
            case 'g':
                $val *= 1024;
                // The 'G' modifier is available since PHP 5.1.0
            case 'm':
                $val *= 1024;
        }

        return $val;
    }
}
