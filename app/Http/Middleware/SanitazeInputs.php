<?php

namespace App\Http\Middleware;

use App\Helpers\Helper;
use Illuminate\Foundation\Http\Middleware\TransformsRequest;

class SanitazeInputs extends TransformsRequest
{
    /**
     * The names of the attributes that should not be sanitaze.
     *
     * @var array
     */
    protected $except = [
        //
    ];

    /**
     * Transform the given value.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return mixed
     */
    protected function transform($key, $value)
    {
        if (in_array($key, $this->except)) {
            return $value;
        }

        return is_string($value) ? Helper::stripTags($value) : $value;
    }
}
