<?php

namespace App\Http\Controllers;

use App\Cierredia;
use App\Reporte;
use Excel;
use Illuminate\Http\Request;

class SafiController extends Controller
{
    /**
     *
     * @param string  [description]
     * @return
     **/
    public function cierredia(Request $request)
    {
        $ventanillasAbiertas = Cierredia::getVentanillasAbiertas();
        $inversionesPendientes = Cierredia::getInversionesPendientes();

        return view('cierredia', [
                'ventanillasAbiertas' => $ventanillasAbiertas,
                'inversionesPendientes' => $inversionesPendientes,
            ]);
    }

    public function respaldarlogs(Request $request)
    {
        Cierredia::respaldarLogs($request->input('step'));

        return response()->json([]);
    }

    public function respaldarbd(Request $request)
    {
        Cierredia::respaldarBD($request->input('step'));

        return response()->json([]);
    }

    public function detenertomcat(Request $request)
    {
        Cierredia::tomcat('stop');

        return response()->json([]);
    }

    public function eliminarlogs(Request $request)
    {
        Cierredia::deleteLogs();

        return response()->json([]);
    }

    public function iniciartomcat(Request $request)
    {
        Cierredia::tomcat('start');

        return response()->json([]);
    }

    public function reporteseguros(Request $request)
    {
        return view('reporteseguros');
    }

    public function reportesegurosexcel(Request $request)
    {
        $reporte = [];
        $meses = [
            1 => 'Enero',
            2 => 'Febrero',
            3 => 'Marzo',
            4 => 'Abril',
            5 => 'Mayo',
            6 => 'Junio',
            7 => 'Julio',
            8 => 'Agosto',
            9 => 'Septiembre',
            10 => 'Octubre',
            11 => 'Noviembre',
            12 => 'Diciembre',
        ];

        if ($request->has('mes') && $request->has('anio')) {
            $reporte = Reporte::getReporteSeguros($request->input('mes'), $request->input('anio'));
        }

        Excel::create('ReporteSeguros'.$meses[$request->input('mes')], function ($excel) use ($reporte) {
            $excel->sheet('Reporte', function ($sheet) use ($reporte) {
                $sheet->setColumnFormat([
                    'G' => '#,##0.00',
                    'H' => '#,##0.00',
                ]);

                $sheet->fromArray($reporte, null, 'A1', false, false);
            });
        })->export('xlsx');
    }
}
