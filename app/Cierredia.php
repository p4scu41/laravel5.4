<?php

namespace App;

use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Cierredia extends Model
{
    protected $connection = 'microfin';

    public static $destino = '/home/administrador/Documentos/Respaldos';

    public static function getVentanillasAbiertas()
    {
        $query = 'SELECT
                SucursalID,
                CajaID,
                TipoCaja,
                UsuarioID,
                DescripcionCaja,
                EstatusOpera
            FROM
                CAJASVENTANILLA
            WHERE
                EstatusOpera = "A"';

        return DB::connection('microfin')->select($query);
    }

    public static function getInversionesPendientes()
    {
        $query = 'SELECT
                INVERSIONES.CuentaAhoID,
                INVERSIONES.ClienteID,
                INVERSIONES.TipoInversionID,
                INVERSIONES.FechaInicio,
                INVERSIONES.Monto,
                INVERSIONES.Plazo,
                INVERSIONES.Estatus,
                INVERSIONES.UsuarioID,
                INVERSIONES.Etiqueta,
                CLIENTES.NombreCompleto AS Cliente,
                USUARIOS.NombreCompleto AS Usuario,
                SUCURSALES.NombreSucurs
            FROM
                INVERSIONES
            INNER JOIN CLIENTES ON
                CLIENTES.ClienteID = INVERSIONES.ClienteID
            INNER JOIN USUARIOS ON
                USUARIOS.UsuarioID = INVERSIONES.UsuarioID
            INNER JOIN SUCURSALES ON
                SUCURSALES.SucursalID = INVERSIONES.Sucursal
            WHERE
                INVERSIONES.Estatus = "A"';

        return DB::connection('microfin')->select($query);
    }

    public static function respaldarLogs($step)
    {
        $folder = self::$destino.'/'.date('Ymd');
        $origen = '/opt/tomcat6/logs/*.*';

        if ($step == 'antescierre') {
            $folder .= 'SinCierre';
        } elseif ($step == 'despuescierre') {
            $folder .= 'ConCierre';
        }

        // Checar si existe o no la carpeta, si no existe la creamos
        if (!file_exists($folder)) {
            mkdir($folder, 0777);
        }

        $destino = $folder.'/Logs';

        // Checar si existe o no la carpeta, si no existe la creamos
        if (!file_exists($destino)) {
            mkdir($destino, 0777);
        }

        $command = 'cp -r '.$origen.' '.$destino.'/';

        exec($command);
    }

    public static function respaldarBD($step)
    {
        $folder = self::$destino.'/'.date('Ymd');
        $sufix  = '';

        if ($step == 'antescierre') {
            $folder .= 'SinCierre';
            $sufix .= 'SinCierre';
        } elseif ($step == 'despuescierre') {
            $folder .= 'ConCierre';
            $sufix .= 'ConCierre';
        }

        // Checar si existe o no la carpeta, si no existe la creamos
        if (!file_exists($folder)) {
            mkdir($folder, 0777);
        }

        $destino = $folder.'/BD';

        // Checar si existe o no la carpeta, si no existe la creamos
        if (!file_exists($destino)) {
            mkdir($destino, 0777);
        }

        $command = 'mysqldump --user=root --password=R3alF1nanciera --host=localhost --protocol=tcp --port=3306 --default-character-set=utf8 --single-transaction=TRUE --routines --events microfin > '.$destino.'/Dump'.date('Ymd').$sufix.'.sql';

        exec($command);
    }

    public static function tomcat($step)
    {
        $command = './opt/tomcat6/bin/';

        if ($step == 'stop') {
            $command .= 'shutdown.sh';
        } elseif ($step == 'start') {
            $command .= 'startup.sh';
        } else {
            throw new Exception('Operación no permitida.', 500);
        }

        exec($command);
    }

    public static function deleteLogs()
    {
        exec('rm –r /opt/tomcat6/logs/*.*');
        exec('rm –r /opt/tomcat6/temp/*.*');
        exec('rm –r /opt/tomcat6/work/*.*');
    }
}
