<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Reporte extends Model
{
    protected $connection = 'microfin';

    public static function getReporteSeguros($mes, $anio)
    {
        $fechaReporte = Carbon::createFromFormat('Y-m-d', $anio.'-'.str_pad($mes, 2, '0', STR_PAD_LEFT).'-01')->lastOfMonth()->format('Y-m-d');
        $query = 'CALL REGDESCAPTACIONREP("'.$fechaReporte.'", 1, 2, 1, 1, "'.date('Y-m-d').'","127.0.0.1", NULL, 1, 0)';
        $resultFiltros = collect(DB::connection('microfin')->select($query))
            ->where('TipoPersona', 1)
            ->whereIn('TipoProducto', [3,9])
            ->where('SaldoFinal', '>=', 3000)
            ->sortBy(function ($item) {
                return $item->ApellidoPat.$item->ApellidoMat.$item->Nombre;
            });
        $date = new Carbon();
        $reporte = [];
        $reporte[] = [
            'No'                => 'No.',
            'NumeroCuenta'      => 'No. De Ahorrador',
            'ApellidoPat'       => 'Apellido Paterno',
            'ApellidoMat'       => 'Apellido Materno',
            'Nombre'            => 'Nombre(s)',
            'FechaNac'          => 'Fecha de Nacimiento',
            'SaldoFinal'        => 'Monto de Ahorro',
            'Cuota'             => 'Cuota',
            'FechaApertura'     => 'Fecha de Inicio de Ahorro',
            'FechaAlta'         => 'Fecha de Alta',
            'FUHO'              => 'FUHO',
            'Sucursal'          => 'Sucursal',
            'Edad'              => 'Edad',
            'NumIdentificacion' => 'NumIdentificacion',
            'TipoPersona'       => 'TipoPersona',
            'RFC'               => 'RFC',
            'CURP'              => 'CURP',
            'TipoProducto'      => 'TipoProducto',
            'TipoModalidad'     => 'TipoModalidad',
            'TasaInteres'       => 'TasaInteres',
            'Plazo'             => 'Plazo',
            'FechaVencim'       => 'FechaVencim',
            'SaldoIniPer'       => 'SaldoIniPer',
            'MtoDepositos'      => 'MtoDepositos',
            'MtoRetiros'        => 'MtoRetiros',
            'FecUltMov'         => 'FecUltMov',
        ];
        $i = 1;

        foreach ($resultFiltros as $fila) {
            $esEmpleadoCorporativo = self::esEmpleadoCorporativo($fila->NumIdentificacion);

            $reporte[] = [
                'No'                => $i++,
                'NumeroCuenta'      => $fila->NumeroCuenta,
                'ApellidoPat'       => $fila->ApellidoPat,
                'ApellidoMat'       => $fila->ApellidoMat,
                'Nombre'            => $fila->Nombre,
                'FechaNac'          => self::strToDate($fila->FechaNac),
                'SaldoFinal'        => $fila->SaldoFinal,
                'Cuota'             => $esEmpleadoCorporativo ? self::getPrimaSeguroPersonalCorporativo($fila->SaldoFinal) : self::getPrimaSeguroPublicoGeneral($fila->SaldoFinal),
                'FechaApertura'     => self::strToDate($fila->FechaApertura),
                'FechaAlta'         => '',
                'FUHO'              => $esEmpleadoCorporativo ? self::getPrimaFUHOPersonalCorporativo($fila->SaldoFinal) : self::getPrimaFUHOPublicoGeneral($fila->SaldoFinal),
                'Sucursal'          => $fila->Sucursal,
                'Edad'              => $date->diffInYears(Carbon::createFromFormat('d-m-Y', self::strToDate($fila->FechaNac))),
                'NumIdentificacion' => $fila->NumIdentificacion,
                'TipoPersona'       => $fila->TipoPersona,
                'RFC'               => $fila->RFC,
                'CURP'              => $fila->CURP,
                'TipoProducto'      => $fila->TipoProducto,
                'TipoModalidad'     => $fila->TipoModalidad,
                'TasaInteres'       => $fila->TasaInteres,
                'Plazo'             => $fila->Plazo,
                'FechaVencim'       => self::strToDate($fila->FechaVencim),
                'SaldoIniPer'       => $fila->SaldoIniPer,
                'MtoDepositos'      => $fila->MtoDepositos,
                'MtoRetiros'        => $fila->MtoRetiros,
                'FecUltMov'         => self::strToDate($fila->FecUltMov),
            ];
        }

        return $reporte;
    }

    public static function strToDate($str)
    {
        // Para el formato YYYY-MM-DD
        $str = str_replace('-', '', $str);

        // Para el formato YYYYMMDD
        return substr($str, 6, 2).'-'.substr($str, 4, 2).'-'.substr($str, 0, 4);
    }


    public static function esEmpleadoCorporativo($id)
    {
        // Aqui se agrega los ID de los promotores de Vista Hermosa
        // $promotoresVistaHermosa = '8';
        // $query = 'SELECT COUNT(*) AS total FROM CLIENTES WHERE ClienteID = '.$id.' AND PromotorActual IN (8)';
        $query = 'SELECT COUNT(*) AS total FROM CLIENTES WHERE ClienteID = '.$id.
            ' AND (LugardeTrabajo LIKE "%ALTERNATIVA%" OR LugardeTrabajo LIKE "%ACCION Y EVOLUCION%")';
        $result = DB::connection('microfin')->select($query);

        return $result[0]->total > 0 ? true : false;
    }

    public static function getPrimaSeguroPersonalCorporativo($saldo)
    {
        $prima = 0;

        if ($saldo >= 3000 && $saldo < 12000) {
            $prima = 5000;
        } elseif ($saldo >= 12000 && $saldo < 17000) {
            $prima = 50000;
        } elseif ($saldo >= 17000) {
            $prima = 100000;
        }

        return $prima;
    }

    public static function getPrimaSeguroPublicoGeneral($saldo)
    {
        $prima = 0;

        if ($saldo >= 3000 && $saldo < 15000) {
            $prima = 5000;
        } elseif ($saldo >= 15000 && $saldo < 25000) {
            $prima = 50000;
        } elseif ($saldo >= 25000) {
            $prima = 100000;
        }

        return $prima;
    }

    public static function getPrimaFUHOPersonalCorporativo($saldo)
    {
        return floor($saldo / 1500) * 75;
    }

    public static function getPrimaFUHOPublicoGeneral($saldo)
    {
        return floor($saldo / 1500) * 50;
    }
}
