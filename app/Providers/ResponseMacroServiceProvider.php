<?php

namespace app\Providers;

use App\Helpers\Helper;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\ServiceProvider;
use Response;

class ResponseMacroServiceProvider extends ServiceProvider
{
    /**
     * Codigos de respuesta http válidos
     *
     * @var array
     */
    public static $code_http_response = [
        // Informational 1xx
        100 => 'Continue',
        101 => 'Switching Protocols',
        102 => 'Processing',
        // Success 2xx
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        203 => 'Non-Authoritative Information',
        204 => 'No Content',
        205 => 'Reset Content',
        206 => 'Partial Content',
        207 => 'Multi-Status',
        // Redirection 3xx
        300 => 'Multiple Choices',
        301 => 'Moved Permanently',
        302 => 'Found', // 1.1
        303 => 'See Other',
        304 => 'Not Modified',
        305 => 'Use Proxy',
        // 306 is deprecated but reserved
        306 => 'Switch Proxy',
        307 => 'Temporary Redirect',
        // Client Error 4xx
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Timeout',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Failed',
        413 => 'Request Entity Too Large',
        414 => 'Request-URI Too Long',
        415 => 'Unsupported Media Type',
        416 => 'Requested Range Not Satisfiable',
        417 => 'Expectation Failed',
        418 => 'I\'m a teapot',
        422 => 'Data Validation Failed.', //Unprocessable Entity
        423 => 'Locked',
        424 => 'Failed Dependency',
        425 => 'Unordered Collection',
        426 => 'Upgrade Required',
        449 => 'Retry With',
        450 => 'Blocked by Windows Parental Controls',
        // Server Error 5xx
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
        503 => 'Service Unavailable',
        504 => 'Gateway Timeout',
        505 => 'HTTP Version Not Supported',
        506 => 'Variant Also Negotiates',
        507 => 'Insufficient Storage',
        509 => 'Bandwidth Limit Exceeded',
        510 => 'Not Extended',
    ];

    /**
     * Formato de la respuesta json
     * @var array
     */
    public static $json_format = [
        // Datos extras que se requieran
        'extra'   => null,
        // Datos solicitados
        'data'    => [],
        // Mensaje breve descriptivo de la respuesta
        'message' => null,
        // Código de la respuesta HTTP
        'status'  => null,
        // Código del error
        'error' => null
    ];

    /**
     * Return the response format with data content
     *
     * @param  array $data    Format ResponseMacroServiceProvider::$json_format
     * @param  int   $status  Http status code
     * @param  array $headers Extra headers
     * @return response()->json()
     */
    public static function responseJson($data, $status, $headers)
    {
        $content = array_merge(static::$json_format, $data);

        if (empty($content['status'])) {
            $content['status'] = $status;
        }

        return response()->json($content, $status, $headers);
    }

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        /**
         * Return the response format with data content
         *
         * @param  array $data    Format ResponseMacroServiceProvider::$json_format
         * @param  int   $status  Http status code
         * @param  array $headers Extra headers
         * @return ResponseMacroServiceProvider::responseJson()
         */
        Response::macro('jsonFormat', function ($data = [], $status, $headers = []) {
            return ResponseMacroServiceProvider::responseJson($data, $status, $headers);
        });

        /**
         * Return the response format with data content
         *
         * @param  array $data    Format ResponseMacroServiceProvider::$json_format
         * @param  int   $status  200 OK
         * @param  array $headers Extra headers
         * @return ResponseMacroServiceProvider::responseJson()
         */
        Response::macro('jsonSuccess', function ($data = [], $status = 200, $headers = []) {
            if (Helper::isNotSetOrEmpty($data, 'message')) {
                $data['message'] = 'Solicitud procesada exitosamente';
            }

            return ResponseMacroServiceProvider::responseJson($data, $status, $headers);
        });

        /**
         * Return the response format with data content and http status code 404 Not Found
         *
         * @param  array $data    Format ResponseMacroServiceProvider::$json_format
         * @param  array $headers Extra headers
         * @return ResponseMacroServiceProvider::responseJson()
         */
        Response::macro('jsonNotFound', function ($data = [], $headers = []) {
            if (Helper::isNotSetOrEmpty($data, 'error')) {
                $data['error'] = 404;
            }

            if (Helper::isNotSetOrEmpty($data, 'message')) {
                $data['message'] = 'Recurso no encontrado';
            }

            return ResponseMacroServiceProvider::responseJson($data, 404, $headers);
        });

        /**
         * Return the response format with data content and http status code 403 Forbidden
         *
         * @param  array $data    Format ResponseMacroServiceProvider::$json_format
         * @param  array $headers Extra headers
         * @return ResponseMacroServiceProvider::responseJson()
         */
        Response::macro('jsonForbidden', function ($data = [], $headers = []) {
            if (Helper::isNotSetOrEmpty($data, 'error')) {
                $data['error'] = 403;
            }

            if (Helper::isNotSetOrEmpty($data, 'message')) {
                $data['message'] = 'Acceso no autorizado';
            }

            return ResponseMacroServiceProvider::responseJson($data, 403, $headers);
        });

        /**
         * Return the response format with data content and http status code 422
         * Data Validation Failed, Unprocessable Entity
         *
         * @param  array $data    Format ResponseMacroServiceProvider::$json_format
         * @param  array $headers Extra headers
         * @return ResponseMacroServiceProvider::responseJson()
         */
        Response::macro('jsonInvalidData', function ($data = [], $headers = []) {
            if (Helper::isNotSetOrEmpty($data, 'error')) {
                $data['error'] = 422;
            }

            if (Helper::isNotSetOrEmpty($data, 'message')) {
                $data['message'] = 'Datos incorrectos';
            }

            return ResponseMacroServiceProvider::responseJson($data, 422, $headers);
        });

        /**
         * Return the response format with data content and the exception
         * If the $e->getCode() is a valid http status code the response is return
         * with this code, in oder hand, the http status code is set with 500
         *
         * @param  \Exception $e       Exception
         * @param  array      $data    Format ResponseMacroServiceProvider::$json_format
         * @param  array      $headers Extra headers
         * @return ResponseMacroServiceProvider::responseJson()
         */
        Response::macro('jsonException', function ($e, $data = [], $headers = []) {
            $status = Helper::isNotSetOrEmpty(
                ResponseMacroServiceProvider::$code_http_response,
                $e->getCode()
            ) ? 500 : $e->getCode();

            if (Helper::isNotSetOrEmpty($data, 'status')) {
                $data['status'] = $status;
            }

            if (Helper::isNotSetOrEmpty($data, 'error')) {
                $data['error'] = $e->getCode();
            }

            if (Helper::isNotSetOrEmpty($data, 'message')) {
                $data['message'] = !empty($e->getMessage()) ? $e->getMessage() : 'Error al procesar los datos';
            }

            if (Helper::isNotSetOrEmpty($data, 'extra')) {
                $data['extra']['message'] = $e->getMessage();

                if (config('app.debug')) {
                    $data['extra']['file'] = $e->getFile();
                    $data['extra']['line'] = $e->getLine();
                    //$data['extra']['trace'] = Helper::array_filter_recursive($e->getTrace());
                }
            }

            if (config('app.debug')) {
                Log::debug($e->getFile() . ' [' . $e->getLine() .']: ' . $e->getMessage());
                Log::debug(request()->input());
                Log::debug($e->getTraceAsString());
            }

            return ResponseMacroServiceProvider::responseJson($data, $status, $headers);
        });

        /**
         * Return the response format with data content and the JWTException
         *
         * @param  Tymon\JWTAuth\Exceptions\JWTException $e    Exception
         * @param  array      $data    Format ResponseMacroServiceProvider::$json_format
         * @param  array      $headers Extra headers
         * @return ResponseMacroServiceProvider::responseJson()
         */
        Response::macro('jsonJwtException', function ($e, $data = [], $headers = []) {
            $status = $e->getStatusCode();

            if (Helper::isNotSetOrEmpty($data, 'status')) {
                $data['status'] = $status;
            }

            if (Helper::isNotSetOrEmpty($data, 'error')) {
                $data['error'] = $e->getStatusCode();
            }

            if (Helper::isNotSetOrEmpty($data, 'message')) {
                $data['message'] = $e->getMessage();
            }

            if (Helper::isNotSetOrEmpty($data, 'extra')) {
                $data['extra']['message'] = $e->getMessage();

                if (config('app.debug')) {
                    $data['extra']['file'] = $e->getFile();
                    $data['extra']['line'] = $e->getLine();
                    //$data['extra']['trace'] = Helper::array_filter_recursive($e->getTrace());
                }
            }

            if (config('app.debug')) {
                Log::debug($e->getFile() . ' [' . $e->getLine() .']: ' . $e->getMessage());
                Log::debug(request()->input());
                Log::debug($e->getTraceAsString());
            }

            return ResponseMacroServiceProvider::responseJson($data, $status, $headers);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
