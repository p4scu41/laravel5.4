<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::directive('boolean', function ($expression) {
            return "<?php echo $expression ? 'Si' : 'No'; ?>";
        });

        Blade::directive('boolean_thumb', function ($expression) {
            return "<?php echo $expression ?
                '<i class=\"fa fa-lg fa-thumbs-up text-success\" data-toggle=\"tooltip\" title=\"Si\"></i>' :
                '<i class=\"fa fa-lg fa-thumbs-o-down text-danger\" data-toggle=\"tooltip\" title=\"No\"></i>';?>";
        });

        Blade::directive('date', function ($expression) {
            return "<?php
                if ($expression != null) {
                    if (is_string({$expression})) {
                        echo \Carbon\Carbon::createFromFormat('Y-m-d', {$expression})->format('d-m-Y');
                    } else {
                        echo {$expression}->format('d-m-Y');
                    }
                }
            ?>";
        });

        Blade::directive('time', function ($expression) {
            return "<?php
                if ($expression != null) {
                    if (is_string({$expression})) {
                        echo \Carbon\Carbon::createFromFormat('H:i:s', {$expression})->format('h:i a');
                    } else {
                        echo {$expression}->format('h:i a');
                    }
                }
            ?>";
        });

        Blade::directive('time_long', function ($expression) {
            return "<?php
                if ($expression != null) {
                    if (is_string({$expression})) {
                        echo \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', {$expression})->format('H:i');
                    } else {
                        echo {$expression}->format('Y-m-d H:i');
                    }
                }
            ?>";
        });

        Blade::directive('datetime', function ($expression) {
            return "<?php
                if ($expression != null) {
                    if (is_string({$expression})) {
                        echo \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', {$expression})->format('d/m/Y h:i a');
                    } else {
                        echo {$expression}->format('d/m/Y h:i a');
                    }
                }
            ?>";
        });

        Blade::directive('datetime_short', function ($expression) {
            return "<?php
                if ($expression != null) {
                    if (is_string({$expression})) {
                        echo \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', {$expression})->format('d-m-Y');
                    } else {
                        echo {$expression}->format('d-m-Y');
                    }
                }
            ?>";
        });

        // Seguridad: Obliga a utilizar SSL
        // \URL::forceSchema('https');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
