@extends('layouts.app')

@push('scripts_body')

@endpush

@push('scripts_body')

@endpush

@section('content')
    <h1 class="page-header">Reporte Seguros de Vida</h1>

    <div class="row">

        <div class="text-center">
            <form action="{{ url('reportesegurosexcel') }}" method="POST" role="form" class="form-inline" target="_blank">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="mes">Mes</label>
                    <select name="mes" id="input" class="form-control" required="required">
                        <option value="">Mes</option>
                        <option value="1">Enero</option>
                        <option value="2">Febrero</option>
                        <option value="3">Marzo</option>
                        <option value="4">Abril</option>
                        <option value="5">Mayo</option>
                        <option value="6">Junio</option>
                        <option value="7">Julio</option>
                        <option value="8">Agosto</option>
                        <option value="9">Septiembre</option>
                        <option value="10">Octubre</option>
                        <option value="11">Noviembre</option>
                        <option value="12">Diciembre</option>
                    </select>
                </div>
                &nbsp;
                <div class="form-group">
                    <label for="anio">Año</label>
                    <input type="number" step="1" class="form-control" name="anio" placeholder="Año" required="required" value="{{ date('Y') }}">
                </div>

                <button type="submit" class="btn btn-primary">Aceptar</button>
            </form>
        </div>

        <p>&nbsp;</p>
    </div>
@endsection
