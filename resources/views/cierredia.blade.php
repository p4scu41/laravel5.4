@extends('layouts.app')

@push('scripts_body')
    <script type="text/javascript">
        var urlRespaldarLogs = '{{ url('respaldarlogs') }}',
            urlRespaldarBd   = '{{ url('respaldarbd') }}',
            urlDetenerTomcat = '{{ url('detenertomcat') }}',
            urlEliminarLogs  = '{{ url('eliminarlogs') }}',
            urlIniciarTomcat = '{{ url('iniciartomcat') }}';
    </script>
@endpush

@push('scripts_body')
    <script src="{{ asset('js/cierredia.js') }}"></script>
@endpush

@section('content')
    <h1 class="page-header">Cierre de día.</h1>

    <div class="row">
        <ol>
            <li>
                <strong>Validaciones</strong>
                <ol>
                    <li>
                        <strong>Usuarios Conectados</strong>

                        <p>Ir a SAFI Menú → Admon → Especiales → Sesiones Activas</p>
                    </li>
                    <li>
                        <strong>Ventanillas Abiertas</strong>

                        <ul>
                            @foreach ($ventanillasAbiertas as $ven)
                                <li>{{ $ven->DescripcionCaja }}</li>
                            @endforeach
                        </ul>
                    </li>
                    <li>
                        <strong>Inversiones Pendientes de Aprobar</strong>

                        <ul>
                            @foreach ($inversionesPendientes as $inv)
                                <li>
                                    Sucursal: {{ $inv->NombreSucurs }},
                                    Cliente: {{ $inv->Cliente }},
                                    Monto: {{ number_format($inv->Monto) }},
                                    Usuario: {{ $inv->Usuario }}
                                </li>
                            @endforeach
                        </ul>
                    </li>
                </ol>
            </li>
            <li>
                <strong>Respaldar Logs de Tomcat</strong>
                <p>
                    <button type="button" class="btn btn-primary" id="btnRespaldarLogsSinCierre">
                        Respaldar Ahora
                    </button>
                </p>
                <div class="alert alert-info" id="alertRespaldarLogsSinCierre" style="display: none;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <p>Respaldando Logs Sin Cierre</p>
                </div>
            </li>
            <li>
                <strong>Respaldar Base de Datos MySQL</strong>
                <p>
                    <button type="button" class="btn btn-primary" id="btnRespaldarBDSinCierre">
                        Respaldar Ahora
                    </button>
                </p>
                <div class="alert alert-info" id="alertRespaldarBDSinCierre" style="display: none;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <p>Respaldando Base de Datos Sin Cierre</p>
                </div>
            </li>
            <li>
                <strong>Cierre de Día</strong>
                <p>
                    Ir al SAFI Menú → Admon → Especiales → Cierre de Día
                </p>
            </li>
            <li>
                <strong>Detener Tomcat</strong>
                <p>
                    <button type="button" class="btn btn-primary" id="btnDetenerTomcat">
                        Detener Tomcat Ahora
                    </button>
                </p>
                <div class="alert alert-info" id="alertDetenerTomcat" style="display: none;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <p>Deteniendo Tomcat</p>
                </div>
            </li>
            <li>
                <strong>Respaldar Logs de Tomcat</strong>
                <p>
                    <button type="button" class="btn btn-primary" id="btnRespaldarLogsConCierre">
                        Respaldar Ahora
                    </button>
                </p>
                <div class="alert alert-info" id="alertRespaldarLogsConCierre" style="display: none;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <p>Respaldando Logs Con Cierre</p>
                </div>
            </li>
            <li>
                <strong>Respaldar Base de Datos MySQL</strong>
                <p>
                    <button type="button" class="btn btn-primary" id="btnRespaldarBDConCierre">
                        Respaldar Ahora
                    </button>
                </p>
                <div class="alert alert-info" id="alertRespaldarBDConCierre" style="display: none;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <p>Respaldando Base de Datos Con Cierre</p>
                </div>
            </li>
            <li>
                <strong>Eliminar Archivos Logs</strong>
                <p>
                    <button type="button" class="btn btn-primary" id="btnEliminarLogs">
                        Eliminar Logs Ahora
                    </button>
                </p>
                <div class="alert alert-info" id="alertEliminarLogs" style="display: none;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <p>Eliminando Archivos Logs</p>
                </div>
            </li>
            <li>
                <strong>Iniciar Tomcat</strong>
                <p>
                    <button type="button" class="btn btn-primary" id="btnIniciarTomcat">
                        Iniciar Tomcat Ahora
                    </button>
                </p>
                <div class="alert alert-info" id="alertIniciarTomcat" style="display: none;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <p>Iniciando Tomcat</p>
                </div>
            </li>
        </ol>
    </div>
@endsection
