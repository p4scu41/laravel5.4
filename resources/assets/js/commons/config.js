window.config = {
    // Habilita el seguimiento de errores
    'debug': true,
    // Icono que se muestra en los botones para indicar que se esta procesando la solicitud
    spinner: '<i class="fa fa-pulse fa-spinner fa-lg"></i>',
    spinner_input: '<i class="fa fa-pulse fa-spinner fa-lg loading-indicator"></i>',
};
