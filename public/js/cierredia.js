'use strict';

/* globals $, config, urlRespaldarLogs, urlRespaldarBd, urlDetenerTomcat, urlEliminarLogs, urlIniciarTomcat */

function sendRequest(btn, url, params) {
    var $this = $(btn),
        cacheBtn = $this.html(),
        $alertResult = $('#' + $this.attr('id').replace('btn', 'alert'));

        $this.html(config.spinner);
        $alertResult.find('p').html('Procesando Solicitud.');
        $alertResult.show();

    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'json',
        data: params,
    })
    .done(function() {
        $alertResult.removeClass('alert-info').addClass('alert-success');
        $alertResult.find('p').html('Solicitud procesada exitosamente.');
    })
    .fail(function(response) {
        $alertResult.removeClass('alert-info').addClass('alert-danger');
        $alertResult.find('p').html('Error al procesar la solicitud. Error: ' +
            response.status + ' ' +
            response.statusText + '. ' +
            response.responseText);
        console.log(response);
    })
    .always(function() {
        $this.html(cacheBtn);
    });
}

$(document).ready(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#btnRespaldarLogsSinCierre').click(function() {
        sendRequest(this, urlRespaldarLogs, {
            step: 'antescierre',
        });
    });

    $('#btnRespaldarBDSinCierre').click(function() {
        sendRequest(this, urlRespaldarBd, {
            step: 'antescierre',
        });
    });

    $('#btnDetenerTomcat').click(function() {
        sendRequest(this, urlDetenerTomcat);
    });

    $('#btnRespaldarLogsConCierre').click(function() {
        sendRequest(this, urlRespaldarLogs, {
            step: 'despuescierre',
        });
    });

    $('#btnRespaldarBDConCierre').click(function() {
        sendRequest(this, urlRespaldarBd, {
            step: 'despuescierre',
        });
    });

    $('#btnEliminarLogs').click(function() {
        sendRequest(this, urlEliminarLogs);
    });

    $('#btnIniciarTomcat').click(function() {
        sendRequest(this, urlIniciarTomcat);
    });

});
