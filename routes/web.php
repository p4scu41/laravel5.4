<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', 'HomeController@index');

Route::group(['middleware' => ['auth']], function () {
    Route::get('/cierredia', 'SafiController@cierredia');
    Route::post('/respaldarlogs', 'SafiController@respaldarlogs');
    Route::post('/respaldarbd', 'SafiController@respaldarbd');
    Route::post('/detenertomcat', 'SafiController@detenertomcat');
    Route::post('/eliminarlogs', 'SafiController@eliminarlogs');
    Route::post('/iniciartomcat', 'SafiController@iniciartomcat');
    Route::get('/reporteseguros', 'SafiController@reporteseguros');
    Route::post('/reportesegurosexcel', 'SafiController@reportesegurosexcel');
});
